var canvas = document.getElementById('myCanvas');
var canvas2 = document.getElementById('colorCanvas');
var ctx = canvas.getContext('2d');
var ctx2 = canvas2.getContext('2d');
var fun = 0;
var pos_x1 = 0, pos_y1 = 0;
var pos_x2 = 0, pos_y2 = 0;
var tmp;
var font_size = "20";
var font_type = "Arial";

display_color_img();

function display_color_img() {
    var img1 = new Image();
    img1.crossOrigin = '';
    img1.src = 'https://i.imgur.com/oy3B4VQ.jpg';
    img1.onload = function () {
        canvas2.width = img1.width;
        canvas2.height = img1.height;
        ctx2.drawImage(img1, 0, 0, img1.width, img1.height);
    }
}

var toSelectColor = document.getElementById("color-picker");
var brushColor;
var RGB = "#FFFFFF";
canvas2.addEventListener("mousedown", function (e) {
    var mousePos = getMousePos(canvas2, e);
    brushColor = ctx2.getImageData(mousePos.x, mousePos.y, 1, 1).data;
    RGB = "rgb(" + brushColor[0] + "," + brushColor[1] + "," + brushColor[2] + ")";
});

var brushSlider = document.getElementById("size");
ctx.lineWidth = brushSlider.value;

brushSlider.addEventListener("change", function () {
    ctx.lineWidth = brushSlider.value;
});

var fontSlider = document.getElementById("fontsize");
font_size = fontSlider.value;

fontSlider.addEventListener("change", function () {
    font_size = fontSlider.value;
});

var fontSelect = document.getElementById("fontselect");
var index = fontSelect.selectedIndex;
font_type = fontSelect.options[index].text;

fontSelect.addEventListener("change", function () {
    index = fontSelect.selectedIndex;
    font_type = fontSelect.options[index].text;
    console.log(font_type);
});

function getMousePos(canvas, evt) {
    var rect = canvas.getBoundingClientRect();
    return {
        x: evt.clientX - rect.left,
        y: evt.clientY - rect.top
    };
}

function toReset() {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
}

change_cursor()
function toBrush() {
    fun = 0;
    change_cursor();
}

function toRect() {
    fun = 1;
    change_cursor();
}

function toTri() {
    fun = 2;
    change_cursor()
}

function toCir() {
    fun = 3;
    change_cursor()
}

function toLine() {
    fun = 4;
    change_cursor()
}

function toErase() {
    fun = 5;
    change_cursor()
}

function toText() {
    fun = 6;
    change_cursor()
}

function toDownload(img_can) {
    var image = canvas.toDataURL();
    img_can.href = image;
}

var input = document.getElementById('input');
input.addEventListener('change', function (event) {
    cPush();
    var file = document.querySelector('input[type=file]').files[0];
    var reader = new FileReader();
    if (file) {
        reader.readAsDataURL(file);
    }
    reader.onload = function (e) {
        var result = e.target.result;
        var image = new Image;
        image.src = result;
        image.onload = function () {
            ctx.drawImage(image, 0, 0, image.width, image.height);
            cPush();
        }
    }
});

function change_cursor() {
    if (fun == 0)
        canvas.style.cursor = "url('./pic/brush.png'), pointer";
    else if (fun == 1)
        canvas.style.cursor = "url('./pic/rect.jpg'), pointer";
    else if (fun == 2)
        canvas.style.cursor = "url('./pic/tri.png'), pointer";
    else if (fun == 3)
        canvas.style.cursor = "url('./pic/circle.png'), pointer";
    else if (fun == 5)
        canvas.style.cursor = "url('./pic/eraser.png'), pointer";
    else if (fun == 6)
        canvas.style.cursor = "url('./pic/Text-Box.jpg'), pointer";
    else
        canvas.style.cursor = "url('./pic/init.png'), pointer";
}

function mouseMove(evt) {
    var mousePos = getMousePos(canvas, evt);
    if (fun == 0) {
        ctx.lineTo(mousePos.x, mousePos.y);
        ctx.stroke();
    }
    else if (fun == 1) {
        toReset();
        ctx.putImageData(tmp, 0, 0);
        ctx.strokeRect(pos_x1, pos_y1, mousePos.x - pos_x1, mousePos.y - pos_y1);
    }
    else if (fun == 2) {
        ctx.beginPath();
        ctx.moveTo(pos_x1, pos_y1);
        toReset();
        ctx.putImageData(tmp, 0, 0);
        ctx.lineTo(mousePos.x, mousePos.y);
        ctx.lineTo(mousePos.x + (2 * (pos_x1 - mousePos.x)), mousePos.y);
        ctx.closePath();
        ctx.stroke();
    }
    else if (fun == 3) {
        ctx.beginPath();
        toReset();
        ctx.putImageData(tmp, 0, 0);
        ctx.arc(pos_x1, pos_y1, Math.sqrt(((mousePos.y - pos_y1) * (mousePos.y - pos_y1)) + ((mousePos.x - pos_x1) * (mousePos.x - pos_x1))), 0, Math.PI * 2);
        ctx.stroke();
    }
    else if (fun == 4) {
        ctx.beginPath();
        ctx.moveTo(pos_x1, pos_y1);
        toReset();
        ctx.putImageData(tmp, 0, 0);
        ctx.lineTo(mousePos.x, mousePos.y);
        ctx.stroke();
    }
    else if (fun == 5) {
        ctx.clearRect(mousePos.x, mousePos.y, ctx.lineWidth * 50, ctx.lineWidth * 50);
    }
}

canvas.addEventListener('mousedown', function (evt) {
    tmp = ctx.getImageData(0, 0, 1500, 600);
    ctx.strokeStyle = RGB;
    ctx.strokeFill = RGB;
    ctx.fillStyle = RGB;
    cPush();
    var mousePos = getMousePos(canvas, evt);
    pos_x1 = mousePos.x;
    pos_y1 = mousePos.y;
    if (fun == 0 || fun == 1) {
        ctx.beginPath();

        ctx.moveTo(pos_x1, pos_y1);
    }
    else if (fun == 5) {
        ctx.clearRect(mousePos.x, mousePos.y, ctx.lineWidth + 10, ctx.lineWidth + 10);
    }
    else if (fun == 6) {
        //ctx.font = "30px Arial";
    }
    evt.preventDefault();
    canvas.addEventListener('mousemove', mouseMove, false);
});

canvas.addEventListener('mouseup', function (evt) {
    var mousePos = getMousePos(canvas, evt);
    pos_x2 = mousePos.x;
    pos_y2 = mousePos.y;
    cPush();
    canvas.removeEventListener('mousemove', mouseMove, false);
    if (fun == 1) {
        ctx.rect(pos_x1, pos_y1, pos_x2 - pos_x1, pos_y2 - pos_y1);
        ctx.stroke();
    }
    else if (fun == 2) {
        ctx.lineTo(mousePos.x, mousePos.y);
        ctx.lineTo(mousePos.x + (2 * (pos_x1 - mousePos.x)), mousePos.y);
        ctx.closePath();
        ctx.stroke();
    }
    else if (fun == 3) {
        ctx.arc(pos_x1, pos_y1, Math.sqrt(((mousePos.y - pos_y1) * (mousePos.y - pos_y1)) + ((mousePos.x - pos_x1) * (mousePos.x - pos_x1))), 0, Math.PI * 2);
        ctx.stroke();
    }
    else if (fun == 4) {
        ctx.lineTo(mousePos.x, mousePos.y);
        ctx.stroke();
    }
}, false);

var cPushArray = new Array();
var cStep = -1;

function cPush() {
    cStep++;
    if (cStep < cPushArray.length) { cPushArray.length = cStep; }
    cPushArray.push(canvas.toDataURL());
}

function toUndo() {
    if (cStep > 0) {
        cStep--;
        var canvasPic = new Image();
        canvasPic.src = cPushArray[cStep];
        toReset();
        canvasPic.onload = function () { ctx.drawImage(canvasPic, 0, 0); }
    }
}

function toRedo() {
    if (cStep < cPushArray.length - 1) {
        cStep++;
        var canvasPic = new Image();
        canvasPic.src = cPushArray[cStep];
        toReset();
        canvasPic.onload = function () { ctx.drawImage(canvasPic, 0, 0); }
    }
}
var textBox = document.getElementById("textBox");;
var textFlag = false;
var textContent = "";
canvas.drawing = function (x1, y1, x2, y2, e) {
    if (fun == 6) {
        if (!ctx) {
            return;
        } else {
            // 设置画笔的颜色和大小
            //ctx.fillStyle = "red";  // 填充颜色为红色
            //ctx.strokeStyle = "blue";  // 画笔的颜色
            //ctx.lineWidth = 5;  // 指定描边线的宽度

            ctx.save();
            ctx.beginPath();

            // 写字
            ctx.font = font_size + "px " + font_type;
            ctx.fillText(textContent, parseInt(textBox.style.left), parseInt(textBox.style.top));

            ctx.restore();
            ctx.closePath();
        }
    }
};

canvas.onmousedown = function mouseDownAction(e) {
    if (fun == 6) {
        this.X1 = e.offsetX;  // 鼠标按下时保存当前位置，为起始位置
        this.Y1 = e.offsetY;
        if (textFlag) {
            textContent = textBox.value;
            textFlag = false;
            textBox.style['z-index'] = 1;
            textBox.value = "";
            this.drawing(this.X1, this.Y1);
        } else if (!textFlag) {
            textFlag = true
            textBox.style.left = this.X1 + 'px';
            textBox.style.top = this.Y1 + 'px';
            textBox.style['z-index'] = 6;
        }
    }
};

canvas.onmouseup = function mouseUpAction(e) {
    textFlag = false;
};