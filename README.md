# Software Studio 2020 Spring
## Assignment 01 Web Canvas

Name: 陳泳晗
Student ID: 107062162



### How to use 
just need to click the button under the canvas

Following are the applications of each button:
![](https://i.imgur.com/JOwGhCq.png)![](https://i.imgur.com/MM9qyVa.png)
    use to brush
    - initial state
    - draw the curve with the signals of mousedown and mousemove
    
![](https://i.imgur.com/TA1wsmr.png)![](https://i.imgur.com/59grBW8.png)
    use to erase
    - clear the part which it pass through
    
![](https://i.imgur.com/KLcrKX1.png)![](https://i.imgur.com/tpRdt4z.png)
    use to type text
    - double click on the canvas, then you can type the words
    - after typing, must click outside the textbox to keep the typed words
    
![](https://i.imgur.com/A1jfX9H.png)![](https://i.imgur.com/WM5Py7M.png)
    use to draw rectangle

![](https://i.imgur.com/4eSoOzB.png)![](https://i.imgur.com/q5vexRT.png)
    use to draw triangle
    
![](https://i.imgur.com/1yFvN9d.png)![](https://i.imgur.com/Z8rd6DD.png)
    use to draw circle
    
![](https://i.imgur.com/WZBMMM5.png)![](https://i.imgur.com/oGzcm85.png)
    use to draw straight line
    
![](https://i.imgur.com/iX5Z9tF.png)
    use to download image
    
![](https://i.imgur.com/Eho6q0F.png)
    use to upload image to canvas
    
![](https://i.imgur.com/T8IYLdP.png)
    use to undo
    - recover the canvas which is before the nearest action on the canvas
    
![](https://i.imgur.com/IvtdRli.png)
    use to redo
    - recover the canvas which is after the nearest action
    
![](https://i.imgur.com/1lvhXqM.png)
    use to reset
    - clear the canvas
    



### Gitlab page link
    "https://107062162.gitlab.io/AS_01_WebCanvas/"

### Others (Optional)

    這次寫canvas，糟糕的coding style是我真正的敵人，每次都沒辦法順利地debug，因為我主要都是參考網絡上的code，簡直是將各種元素都融合在裡面了，不過導致整個code看起來凌亂不堪，索然我有想辦法整理它，不過因為整個coding style都很糟，最後的結果依然不盡人意 ┭┮﹏┭┮。

<style>
table th{
    width: 100%;
}
</style>